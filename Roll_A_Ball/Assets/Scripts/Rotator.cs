﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
        // Set the x position to loop between -3 and 3
        transform.position = new Vector3(Mathf.PingPong(Time.time,3), transform.position.y, transform.position.z);
    }
}